# Neural Network - training (function)
# step 1 : creation of data (input and output according to the function)
# step 2 : create a model with nn.MCLRegressor
# step 3 : train the model with the data (model.fit(X, Y))
# step 4 : run the model (input : X_test) and compare the result with our Y_test
# step 5 : calculation of the error

import sklearn.neural_network as nn
import numpy as np

# generation of the data
def data_generation(inf_bound, sup_bound, f, size_list) :
    X = np.array([[np.random.uniform(inf_bound, sup_bound), np.random.uniform(inf_bound, sup_bound), np.random.uniform(inf_bound, sup_bound)] for x in range(size_list)])
    Y = np.array([f(X[i][0], X[i][1], X[i][2]) for i in range(size_list)])
    return X, Y

# definition of the function
def f(x0, x1, x2):
    Y = np.array([x0**3*(np.sin(2*x1))**2, x2**2+x0])
    return np.transpose(Y)


if __name__ == "__main__":
    inf_bound = 0
    sup_bound = np.pi
    size_list = 10000
    size_list_test = 50
    X, Y = data_generation(inf_bound, sup_bound, f, size_list)


# creation of the model
    model = nn.MLPRegressor(hidden_layer_sizes=(5, 5), activation="tanh", solver="lbfgs", max_iter = 200)

# train the model
    model.fit(X, Y)

# generation of data_set_TEST
    X_test, Y_test = data_generation(inf_bound,sup_bound, f, size_list_test)

# use this data_set_TEST with the model
    Y_predict = model.predict(X_test)

# calculation of the error
    errors = np.abs(Y_test - Y_predict)
    print('Mean test error :', np.mean(errors[:, 0]))
