import sklearn.neural_network as nn
import numpy as np

def f(x):
    return x**2 + np.sin(5*x)

# creation of synthetic data
def creation_data_function(inf_bound, sup_bound, f, size_list ) :
    X = np.array([[np.random.uniform(inf_bound, sup_bound)] for x in range(size_list)])
    Y = np.array([f(x) for x in X])
    return X, Y

def main(inf_bound, sup_bound, f, N_training, N_test) :
    # creation of training_set
    X, Y = creation_data_function(inf_bound, sup_bound, f, N_training)

    # creation of the model
    model = nn.MLPRegressor(hidden_layer_sizes=(10, 10, 10, 10), activation='tanh', solver='lbfgs', max_iter=100000)
    model.fit(X, Y)

    # creation test_set
    X_test, Y_test = creation_data_function(inf_bound, sup_bound, f, N_test)

    # prediction_set
    Y_pred = model.predict(X_test)
    print(Y_pred)
    Y_pred = np.reshape(Y_pred, (N_test, 1))
    print(Y_pred)

    # calculation of the error by comparing prediction and test
    error = np.abs(Y_test - Y_pred)

    print("error : ", np.mean(error[:, 0]))
    return error

if __name__ == "__main__":
    inf_bound = 0
    sup_bound = np.pi
    N_test = 5
    N_training = 20
    error = main(inf_bound, sup_bound, f, N_training, N_test)



